/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare module 'leaflet';

declare module 'leaflet/dist/images/marker-icon-2x.png'
declare module 'leaflet/dist/images/marker-icon.png'
declare module 'leaflet/dist/images/marker-shadow.png'